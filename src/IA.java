
public class IA {

	final static float minR = 0.02f;
	final static float maxR = 0.96f;
	
	final static float beta = 17169f;
	final static float K = 748.66f;
	final static float alpha = 1.2772f;

	
	public static float IA() {
		double r = (Math.random() * (maxR - minR)) + minR;
		
		return inverseCDF(r);
	}

	private static float inverseCDF(double r) {
		//Separo un poco los miembros de la funcion para poder codearlo mas tranquilo
		float a = (float) (Math.pow((1-r), -1 / K) -1);
		float b = 1 / alpha;
		
		return (float) (beta * Math.pow(a, b));
	}
	
}
