public class Simulacion {

	//---------------Variables de control---------------
		static int N = 2; //Cantidad de carriles
		static int K1 = 9; //Cantidad de ns para que se abra el carril 2
		static int K2 = 10; //Cantidad de ns para que se abra el carril 3
		static int K3 = 12; //Cantidad de ns para que se abra el carril 4
		static int tFinal = 999999;
	//---------------Variables control.
			
	//---------------Variables globales---------------
	static float t;
	static float sps;
	static float [] tpi = new float [N];
	static float tpll;
	static int nt, ns;
	static float sta;
	static int [] cantParaAbrirCarril;
	//---------------Variables globales.
	
	static int HV = 9999999;
	
	public static void main(String[] args) {
		/*
			Aca hago muchas corridas de la misma simulacion
			para despues promedio PEC
		*/
		
		float sumatoriaPEC = 0f;
		int cantidad_de_corridas = 300;
		
		for(int i = 0; i< cantidad_de_corridas; i++) {
			sumatoriaPEC += simulacion();
		}
		
		System.out.println("PEC promedio = " + sumatoriaPEC / cantidad_de_corridas);
	}
	
	
	//Esta es la metodologia en si
	private static float simulacion() {
		condicionesIniciales();
		
		while(t <= tFinal) {
			cicloDeSimulacion();
		}
	
		tpll = HV;
		while(ns >= 1) {
			cicloDeSimulacion();
		}
		
		float PEC = (sps - sta) / nt;
		return PEC;
	}
	
	private static void condicionesIniciales() {
		t = 0;
		sps = 0;
		sta = 0;
		for(int i = 0; i<tpi.length; i++) {
			tpi[i] = HV;
		}
		tpll = 0;
		nt = 0;
		ns = 0;
		cantParaAbrirCarril = new int[] {1, K1, K2, K3};
	}

	private static void cicloDeSimulacion() {
		int i = minTpi();
		
		if(tpll <= tpi[i]) {
			procesarLlegada();
		} else {
			procesarSalida(i);
		}
	}

	private static void procesarSalida(int i) {
		sps += (tpi[i] - t) * ns;
		t = tpi[i];
		
		ns--;

		if(ns >= cantParaAbrirCarril[i]) {
			float ta = TA.TA();
			tpi[i] = t + ta;
			sta += ta;
		} else {
			tpi[i] = HV;
		}
		
		nt++;
	}

	private static void procesarLlegada() {
		sps += (tpll - t) * ns;
		t = tpll;
	
		tpll = t + IA.IA();
		ns++;
		
		for(int j = 0; j<N; j++) {
			if(ns == cantParaAbrirCarril[j]) {
				float ta = TA.TA();
				tpi[j] = t + ta;
				sta += ta;
			}
		}
		
	}
	
	private static int minTpi() {
		int minTPI = 0;
		for(int i = 1; i<N; i++) {
			if(tpi[i] < tpi[minTPI]) {
				minTPI = i;
			}
		}
		
		return minTPI;
	}
		
}
