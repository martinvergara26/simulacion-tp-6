
public class TA {
	
	final static float minR = 0.01f;
	final static float maxR = 0.98f;
	
	final static float alpha = 3.3398f;
	final static float beta = 80.499f;
	final static float gamma = -9.6413f;
	
	public static float TA() {
		double r = (Math.random() * (maxR - minR)) + minR;
		
		return inverseCDF(r);
	}
	
	private static float inverseCDF(double r) {
		//Separo un poco los miembros de la funcion para poder codearlo mas tranquilo
		float m = (float) Math.pow(((1/r) - 1), (1/alpha));
		float k = beta + (m * gamma);
		
		return m * k;
	}

}
